package com.company.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GmailPage {

    WebDriver webDriver;

    public GmailPage(WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy (css = "input[type='email']")
    WebElement email;

    @FindBy (css = "input[type='password']")
    WebElement password;

    @FindBy (css = "span[class = 'RveJvd snByac']")
    WebElement nextBtn;

    @FindBy (css = "div[class = 'T-I J-J5-Ji T-I-KE L3']")
    WebElement writeBtn;

    @FindBy (css = "a[class = 'WaidBe']")
    WebElement gmailServiceLink;

    @FindBy (css = "div[class = 'nH Hd']")
    WebElement newMsgForm;

    @FindBy (css = "div[class = 'wO nr l1'] textarea")
    WebElement addressee;

    @FindBy (css = "div[class = 'aoD az6'] input")
    WebElement subject;

    @FindBy (css = "div[class = 'Am Al editable LW-avf']")
    WebElement letter;

    @FindBy (css = "div[class = 'J-J5-Ji btA']")
    WebElement sendBtn;


    public WebElement getEmail() {
        return email;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getNextBtn() {
        return nextBtn;
    }

    public WebElement getWriteBtn() {
        return writeBtn;
    }

    public WebElement getGmailServiceLink() {
        return gmailServiceLink;
    }

    public WebElement getNewMsgForm() {
        return newMsgForm;
    }

    public WebElement getAddressee() {
        return addressee;
    }

    public WebElement getSubject() {
        return subject;
    }

    public WebElement getLetter() {
        return letter;
    }

    public WebElement getSendBtn() {
        return sendBtn;
    }

    public void enterAddressee(String email){
        getAddressee().sendKeys(email);
    }

    public void enterSubject(String subject){
        getSubject().sendKeys(subject);
    }

    public void enterLetter(String letter){
        getLetter().sendKeys(letter);
    }
}
