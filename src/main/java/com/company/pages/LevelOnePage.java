package com.company.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LevelOnePage {

    WebDriver webDriver;

    public LevelOnePage(WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//div[@class = 'intro-message']//a[@style]")
    WebElement clickHereLink;

    @FindBy(xpath = "//div[@class = 'intro-message']//h3[1]")
    WebElement successfulMsg;

    @FindBy(css = ".form-control")
    WebElement enterNameForm;

    @FindBy(xpath = ".//input")
    WebElement enterAgeForm;

    @FindBy(css = "select[name = 'gender']")
    WebElement genderForm;

    @FindBy(xpath = ".//form")
    WebElement marriedForm;

    @FindBy(css = "a[class = 'btn btn-warning']")
    WebElement clickHereBtn;

    @FindBy(css = "a[class = 'btn btn-danger btn-lg']")
    WebElement backBtn;

    @FindBy(css = ".form-inline input")
    WebElement boxForText;

    @FindBy(xpath = "(.//div[@class = 'intro-message']/h3)[1]")
    WebElement textForCopy;

    @FindBy(css = "button[class = 'dropbtn']")
    WebElement selectMenu;

    @FindBy(css = "font")
    WebElement newWindSuccessfulMsg;

    @FindBy(css = "a[class ='btn btn-success btn-lg']")
    WebElement finishBtn;



    public WebElement getClickHereLink() {
        return clickHereLink;
    }

    public WebElement getSuccessfulMsg() {
        return successfulMsg;
    }

    public WebElement getEnterNameForm() {
        return enterNameForm;
    }

    public WebElement getEnterAgeForm() {
        return enterAgeForm;
    }

    public WebElement getGenderForm() {
        return genderForm;
    }

    public WebElement getFemale() {
        return genderForm.findElement(By.cssSelector("option[value = '2']"));
    }

    public WebElement getNoMarried(){
        return marriedForm.findElement(By.xpath("//input[contains(@value,'1')]"));
    }

    public WebElement getClickHereBtn() {
        return clickHereBtn;
    }

    public WebElement getTextForCopy() {
        return textForCopy;
    }

    public WebElement getBackBtn() {
        return backBtn;
    }

    public WebElement getBoxForText() {
        return boxForText;
    }

    public WebElement getSelectMenu() {
        return selectMenu;
    }

    public WebElement getNewWindSuccessfulMsg() {
        return newWindSuccessfulMsg;
    }

    public WebElement getFinishBtn() {
        return finishBtn;
    }

    String option = ".//div[@id = 'myDropdown']/a[contains(.,'%s')]";

    public WebElement myObtion(String number){
        return  webDriver.findElement(By.xpath(String.format(option, number)));
    }

    String hobby = ".//label[contains(.,'%s')]";

    public WebElement myHobby(String h){
        return  webDriver.findElement(By.xpath(String.format(hobby, h)));
    }

    public WebElement hobbyCheckbox(String n){
        return webDriver.findElement(By.id(String.format(n)));
    }


    public void enterUserName(String name){
        getEnterNameForm().click();
        getEnterNameForm().sendKeys(name);
    }

    public void enterUserAge(String age){
        getEnterAgeForm().click();
        getEnterAgeForm().clear();
        getEnterAgeForm().sendKeys(age);
    }



}
