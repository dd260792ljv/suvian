package com.company;


import com.company.pages.GmailPage;
import com.company.pages.LevelOnePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Application {

    private WebDriver webDriver = new ChromeDriver();
    private WebDriverWait wait = new WebDriverWait(webDriver, 10);;

    public Application(WebDriver webDriver, WebDriverWait wait) {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
        this.webDriver = webDriver;
        this.wait = wait;
    }

    public LevelOnePage levelOnePage(){
        return new LevelOnePage(webDriver);
    }

    public GmailPage gmailPage(){
        return new GmailPage(webDriver);
    }
}
