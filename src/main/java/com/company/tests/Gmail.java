package com.company.tests;


import com.company.pages.GmailPage;
import com.company.pages.LevelOnePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Gmail  {

    WebDriver webDriver;
    WebDriverWait wait;
    GmailPage gmailPage;

    @Before
    public void before() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
        webDriver = new ChromeDriver();
        wait = new WebDriverWait(webDriver, 30);
        gmailPage = new GmailPage(webDriver);
    }

    @Test
    public void sendEmail(){
        webDriver.get("https://accounts.google.com/");
        gmailPage.getEmail().sendKeys("dd260792ljv@gmail.com");
        gmailPage.getNextBtn().click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[type='password']")));
        gmailPage.getPassword().sendKeys("Dnjhy1r3");
        gmailPage.getNextBtn().click();
        wait.until(ExpectedConditions.visibilityOf(gmailPage.getGmailServiceLink()));
        gmailPage.getGmailServiceLink().click();
        wait.until(ExpectedConditions.visibilityOf(gmailPage.getWriteBtn()));
        gmailPage.getWriteBtn().click();
        wait.until(ExpectedConditions.visibilityOf(gmailPage.getAddressee()));
        gmailPage.enterAddressee("dd260792ljv@gmail.com");
        gmailPage.enterSubject("Hello world");
        gmailPage.enterLetter("Hello world");
        gmailPage.getSendBtn().click();

    }

    @After
    public void after() {webDriver.quit();
    }

}
