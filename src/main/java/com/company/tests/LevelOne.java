package com.company.tests;

import com.company.pages.LevelOnePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LevelOne {

    WebDriver webDriver;
    WebDriverWait wait;
    LevelOnePage levelOnePage;

    @Before
    public void before() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
        webDriver = new ChromeDriver();
        wait = new WebDriverWait(webDriver, 10);
        levelOnePage = new LevelOnePage(webDriver);
    }

    @Test
    public void clickLink() {
        webDriver.get("http://www.suvian.in/selenium/1.1link.html");
        levelOnePage.getClickHereLink().click();
        assertTrue(levelOnePage.getSuccessfulMsg().getText().contains("Link Successfully clicked"));
    }

    @Test
    public void enterName() {
        webDriver.get("http://www.suvian.in/selenium/1.2text_field.html");
        levelOnePage.enterUserName("Julia");
        assertTrue(levelOnePage.getEnterNameForm().getAttribute("value").contains("Julia"));
    }

    @Test
    public void enterAge() {
        webDriver.get("http://www.suvian.in/selenium/1.3age_plceholder.html");
        levelOnePage.enterUserAge("25");
        assertTrue(webDriver.findElement(By.cssSelector(".form-control")).getAttribute("value").contains("25"));
    }

    @Test
    public void selectGender() {
        webDriver.get("http://www.suvian.in/selenium/1.4gender_dropdown.html");
        levelOnePage.getGenderForm().click();
        levelOnePage.getFemale().click();
        assertTrue(levelOnePage.getGenderForm().getAttribute("value").contains("2"));
    }

    @Test
    public void selectMarried() {
        webDriver.get("http://www.suvian.in/selenium/1.5married_radio.html");
        levelOnePage.getNoMarried().click();
        assertTrue(levelOnePage.getNoMarried().isSelected());
    }

    @Test
    public void selectHobby(){
        webDriver.get("http://www.suvian.in/selenium/1.6checkbox.html");
        levelOnePage.myHobby("Sports").click();
        levelOnePage.myHobby("Dancing").click();
        assertTrue(levelOnePage.hobbyCheckbox("2").isSelected());
        assertTrue(levelOnePage.hobbyCheckbox("3").isSelected());
        assertFalse(levelOnePage.hobbyCheckbox("1").isSelected());
        assertFalse(levelOnePage.hobbyCheckbox("4").isSelected());
    }

    @Test
    public void clickButton() throws InterruptedException {
        webDriver.get("http://www.suvian.in/selenium/1.7button.html");
        levelOnePage.getClickHereBtn().click();
        assertTrue(levelOnePage.getSuccessfulMsg().getText().contains("Button Successfully clicked. Wait for 10 seconds on this page and then click on 'Navigate Back' button"));
        Thread.sleep(10000);
        levelOnePage.getBackBtn().click();
        assertTrue(levelOnePage.getClickHereBtn().isDisplayed());
    }

    @Test
    public void copyPasteTextSimple(){
        webDriver.get("http://www.suvian.in/selenium/1.8copyAndPasteText.html");
        String myOrderText = levelOnePage.getTextForCopy().getText();
        levelOnePage.getBoxForText().sendKeys(myOrderText);
        assertTrue(levelOnePage.getBoxForText().getAttribute("value").contains(myOrderText));
    }

    @Test
    public void copyPasteText(){
        webDriver.get("http://www.suvian.in/selenium/1.9copyAndPasteTextAdvanced.html");
        String myOrderText = levelOnePage.getTextForCopy().getText().substring(36,59);
        levelOnePage.getBoxForText().sendKeys(myOrderText);
        assertTrue(levelOnePage.getBoxForText().getAttribute("value").contains(myOrderText));
    }

    @Test
    public void selectOption(){
        webDriver.get("http://www.suvian.in/selenium/1.10selectElementFromDD.html");
        levelOnePage.getSelectMenu().click();
        levelOnePage.myObtion("2").click();

        String parentWindow = webDriver.getWindowHandle();
        for(String winHandle : webDriver.getWindowHandles()){
            if(winHandle != parentWindow) {
                webDriver.switchTo().window(winHandle);
            }
        }

        assertTrue(levelOnePage.getNewWindSuccessfulMsg().getText().contains("Congratulations.. You Selected option 2. Close this browser tab and proceed to end of Level 1."));
        webDriver.close();
        webDriver.switchTo().window(parentWindow);
        assertTrue(levelOnePage.getSelectMenu().isDisplayed());
        levelOnePage.getFinishBtn().click();
        assertTrue(levelOnePage.getSuccessfulMsg().getText().contains("Congratulations on completing Level 1 scenarios. Get ready for Level 2 which is tougher"));
    }

    @After
    public void after() {webDriver.quit();
    }

}
